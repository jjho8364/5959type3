package com.zaffran.o9o9tvgittype3.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.squareup.picasso.Picasso;
import com.zaffran.o9o9tvgittype3.R;
import com.zaffran.o9o9tvgittype3.fragment.FakeFragment;
import com.zaffran.o9o9tvgittype3.fragment.FragmentBayIld;
import com.zaffran.o9o9tvgittype3.fragment.FragmentLinkMid;
import com.zaffran.o9o9tvgittype3.fragment.FragmentLive;
import com.zaffran.o9o9tvgittype3.fragment.FragmentMaru;
import com.zaffran.o9o9tvgittype3.fragment.FragmentMaruSearch;
import com.zaffran.o9o9tvgittype3.fragment.FragmentO9o9;
import com.zaffran.o9o9tvgittype3.fragment.FragmentO9o9Mubi;
import com.zaffran.o9o9tvgittype3.fragment.FragmentQoo;

import java.util.Date;

public class MainActivity extends FragmentActivity implements View.OnClickListener {
    private final String TAG = " MainActivityTAG - ";

    private int mCurrentFragmentIndex;
    public final static int FRAGMENT_ONE = 0;
    public final static int FRAGMENT_TWO = 1;
    public final static int FRAGMENT_THREE = 2;
    public final static int FRAGMENT_FOUR = 3;
    public final static int FRAGMENT_FIVE = 4;
    public final static int FRAGMENT_SIX = 5;
    public final static int FRAGMENT_SEVEN = 6;
    public final static int FRAGMENT_EIGHT = 7;
    public final static int FRAGMENT_NINE = 8;
    public final static int FRAGMENT_TEN = 9;
    public final static int FRAGMENT_ELEVEN = 10;
    public final static int FRAGMENT_TWOWELVE = 11;
    public final static int FRAGMENT_MARU_SEARCH = 28;
    public final static int FRAGMENT_LIVE = 42;
    public final static int FRAGMENT_LIVE_ENTER = 43;
    public final static int FRAGMENT_FAKE_MENU = 45;

    private ImageView iv_fragment01;
    private ImageView iv_fragment02;
    private ImageView iv_fragment03;
    private ImageView iv_fragment04;
    private ImageView iv_fragment05;
    private ImageView iv_fragment06;
    private ImageView iv_fragment07;
    private ImageView iv_fragment08;
    private ImageView iv_fragment09;
    private ImageView iv_fragment10;
    private ImageView iv_fragment11;
    private ImageView iv_fragment12;
    private TextView tv_fragmentLive;
    private TextView tv_fragmentLiveEnter;
    private TextView tv_fragmentFakeMenu;

    private String fragment01Url = "";
    private String fragment02Url = "";
    private String fragment03Url = "";
    private String fragment04Url = "";
    private String fragment05Url = "";
    private String fragment06Url = "";
    private String fragment07Url = "";
    private String fragment08Url = "";
    private String fragment09Url = "";
    private String fragment10Url = "";
    private String fragment11Url = "";
    private String fragment12Url = "";
    private String fragmentLive;
    private String fragmentLiveEnter;
    private String fragmentFakeMenu;

    private String nextAppUrl = "";
    private String mxPlayerUrl = "";
    private Date curDate;
    private String appStatus = "99";

    private int adsCnt = 0;

    private int adn = 0;

    Date d1;
    Date d2;

    Date installedDate;

    // native ads
    private Button refresh;
    //private UnifiedNativeAd nativeAd;

    // alert dialog
    AlertDialog.Builder builder;
    AlertDialog alertDialog;
    View layout;
    TextView finishApp;
    TextView finishAppCancel;

    // fk ads
    private WebView webView;
    private String fkAdsUrl = "";

    private String tistoryUrl = "";
    private String tistoryRndPage = "";

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.custom_dialog, null);

        refresh = layout.findViewById(R.id.btn_refresh);
        finishApp = layout.findViewById(R.id.tv_finish_app);
        finishAppCancel = layout.findViewById(R.id.tv_finish_app_cancel);
        refresh.setOnClickListener(this);
        finishApp.setOnClickListener(this);
        finishAppCancel.setOnClickListener(this);

        // alert dialog
        builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialog = builder.create();

        Intent mainIntent = getIntent();
        appStatus = mainIntent.getStringExtra("appStatus");
        Log.d(TAG, "appStatus : " + appStatus);

        tistoryUrl = mainIntent.getStringExtra("tistoryUrl");
        tistoryRndPage = mainIntent.getStringExtra("tistoryRndPage");

        d1 = new Date();

        SharedPreferences pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언
        String first = pref.getString("first",null);
        long installedTime = pref.getLong("installedTime",0);
        if(first != null && !appStatus.equals("3") && !appStatus.equals("4") && !appStatus.equals("99"))  appStatus = "1";

        //appStatus = "99";

        if(appStatus.equals("99")){
            setContentView(R.layout.activity_main);

            /*HorizontalScrollView horview01 = (HorizontalScrollView)findViewById(R.id.horview01);
            HorizontalScrollView horview02 = (HorizontalScrollView)findViewById(R.id.horview02);
            horview01.setVisibility(View.GONE);
            horview02.setVisibility(View.GONE);*/
            HorizontalScrollView horview01 = (HorizontalScrollView)findViewById(R.id.horview01);
            HorizontalScrollView horview02 = (HorizontalScrollView)findViewById(R.id.horview02);
            HorizontalScrollView horview03 = (HorizontalScrollView)findViewById(R.id.horview03);
            HorizontalScrollView horview04 = (HorizontalScrollView)findViewById(R.id.horview04);
            horview01.setVisibility(View.GONE);
            horview02.setVisibility(View.GONE);
            horview03.setVisibility(View.GONE);
            horview04.setVisibility(View.GONE);

            tv_fragmentFakeMenu = (TextView)findViewById(R.id.tv_fake_menu);

            mCurrentFragmentIndex = FRAGMENT_FAKE_MENU;     // 첫 Fragment 를 초기화
            fragmentReplace(mCurrentFragmentIndex);

            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
            editor.putString("first", "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.putString("adsCnt", "1"); //First라는 key값으로 id 데이터를 저장한다.

            editor.commit(); //완료한다.

        } else if (appStatus.equals("1")){

            setContentView(R.layout.activity_main);

            curDate = new Date();

            HorizontalScrollView horview05 = (HorizontalScrollView)findViewById(R.id.horview05);
            horview05.setVisibility(View.GONE);

            iv_fragment01 = (ImageView)findViewById(R.id.iv_fragment01);
            iv_fragment02 = (ImageView)findViewById(R.id.iv_fragment02);
            iv_fragment03 = (ImageView)findViewById(R.id.iv_fragment03);
            iv_fragment04 = (ImageView)findViewById(R.id.iv_fragment04);
            iv_fragment05 = (ImageView)findViewById(R.id.iv_fragment05);
            iv_fragment06 = (ImageView)findViewById(R.id.iv_fragment06);
            iv_fragment07 = (ImageView)findViewById(R.id.iv_fragment07);
            iv_fragment08 = (ImageView)findViewById(R.id.iv_fragment08);
            iv_fragment09 = (ImageView)findViewById(R.id.iv_fragment09);
            iv_fragment10 = (ImageView)findViewById(R.id.iv_fragment10);
            iv_fragment11 = (ImageView)findViewById(R.id.iv_fragment11);
            iv_fragment12 = (ImageView)findViewById(R.id.iv_fragment12);
            //fragmentFakeMenu = (TextView)findViewById(R.id.tv_fake_menu);

            iv_fragment01.setOnClickListener(this);
            iv_fragment02.setOnClickListener(this);
            iv_fragment03.setOnClickListener(this);
            iv_fragment04.setOnClickListener(this);
            iv_fragment05.setOnClickListener(this);
            iv_fragment06.setOnClickListener(this);
            iv_fragment07.setOnClickListener(this);
            iv_fragment08.setOnClickListener(this);
            iv_fragment09.setOnClickListener(this);
            iv_fragment10.setOnClickListener(this);
            iv_fragment11.setOnClickListener(this);
            iv_fragment12.setOnClickListener(this);

            fragment01Url = mainIntent.getStringExtra("fragment01Url");
            fragment02Url = mainIntent.getStringExtra("fragment02Url");
            fragment03Url = mainIntent.getStringExtra("fragment03Url");
            fragment04Url = mainIntent.getStringExtra("fragment04Url");
            fragment05Url = mainIntent.getStringExtra("fragment05Url");
            fragment06Url = mainIntent.getStringExtra("fragment06Url");
            fragment07Url = mainIntent.getStringExtra("fragment07Url");
            fragment08Url = mainIntent.getStringExtra("fragment08Url");
            fragment09Url = mainIntent.getStringExtra("fragment09Url");
            fragment10Url = mainIntent.getStringExtra("fragment10Url");
            fragment11Url = mainIntent.getStringExtra("fragment11Url");
            fragment12Url = mainIntent.getStringExtra("fragment12Url");
            fragmentLive = mainIntent.getStringExtra("fragLiveTv");
            fragmentLiveEnter = mainIntent.getStringExtra("fragLiveTvEnter");

            mCurrentFragmentIndex = FRAGMENT_MARU_SEARCH;     // 첫 Fragment 를 초기화
            fragmentReplace(mCurrentFragmentIndex);

            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
            editor.putString("first", "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.putString("adsCnt", "1"); //First라는 key값으로 id 데이터를 저장한다.

            editor.commit(); //완료한다.

            // live tv channel
            if(installedTime == 0 && true){
                installedTime = new Date().getTime();
                Log.d(TAG, "first install : " + installedTime);
                editor.putLong("installedTime", installedTime); //First라는 key값으로 id 데이터를 저장한다.
            } else {
                long nowTime = new Date().getTime();
                long diff = nowTime - installedTime;
                long min = diff/1000/60 ;
                long hour = min/60;
                Log.d(TAG, "now - istalled time : " + hour);
                if(hour >= 12) {
                    // fake ads is here
                    tv_fragmentLive.setVisibility(View.VISIBLE);
                    tv_fragmentLiveEnter.setVisibility(View.VISIBLE);
                }
            }
        } else if(appStatus.equals("2")){
            setContentView(R.layout.maintenance);
            String maintenanceImgUrl = mainIntent.getStringExtra("maintenance");
            ImageView imgView = (ImageView)findViewById(R.id.img_maintenance);
            if(maintenanceImgUrl == null || maintenanceImgUrl.equals("")){
                imgView.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(this).load(maintenanceImgUrl).into(imgView);
            }
        } else if(appStatus.equals("3")){
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closed");
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            if(closedImgUrl == null || closedImgUrl.equals("")){
                imgView.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(this).load(closedImgUrl).into(imgView);
            }
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(this);
        } else if(appStatus.equals("4")) {   // mx player


        } else if(appStatus.equals("9")) {   // mx player
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closedImgUrl");
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            Log.d(TAG, "  " + nextAppUrl);
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            Picasso.with(this).load(closedImgUrl).into(imgView);
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent marketLaunch1 = new Intent(Intent.ACTION_VIEW);
                    marketLaunch1.setData(Uri.parse(nextAppUrl));
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(nextAppUrl));
                    startActivity(intent);
                }
            });
        }

        // admob
        MobileAds.initialize(this, getResources().getString(R.string.app_id));
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // native trying
        AdLoader.Builder builder = new AdLoader.Builder( this, getResources().getString(R.string.native_main));

        builder.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
            @Override
            public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                TemplateView template = layout.findViewById(R.id.my_template);
                template.setNativeAd(unifiedNativeAd);
            }
        });

        AdLoader adLoader = builder.build();
        adLoader.loadAd(new AdRequest.Builder().build());

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.iv_fragment01:
                Log.d(TAG, "getID : " + v.getId());
                offColorTv();
                iv_fragment01.setBackgroundResource(R.drawable.drama1);
                mCurrentFragmentIndex = FRAGMENT_ONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.iv_fragment02:
                Log.d(TAG, "getID : " + v.getId());
                offColorTv();
                iv_fragment02.setBackgroundResource(R.drawable.enter1);
                mCurrentFragmentIndex = FRAGMENT_TWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.iv_fragment03:
                offColorTv();
                iv_fragment03.setBackgroundResource(R.drawable.sisa1);
                mCurrentFragmentIndex = FRAGMENT_THREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.iv_fragment04:
                offColorTv();
                iv_fragment04.setBackgroundResource(R.drawable.movie1);
                mCurrentFragmentIndex = FRAGMENT_FOUR;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.iv_fragment05:
                offColorTv();
                iv_fragment05.setBackgroundResource(R.drawable.mid1);
                mCurrentFragmentIndex = FRAGMENT_FIVE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.iv_fragment06:
                offColorTv();
                iv_fragment06.setBackgroundResource(R.drawable.total1);
                mCurrentFragmentIndex = FRAGMENT_SIX;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.iv_fragment07:
                offColorTv();
                iv_fragment07.setBackgroundResource(R.drawable.drama1);
                mCurrentFragmentIndex = FRAGMENT_SEVEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.iv_fragment08:
                offColorTv();
                iv_fragment08.setBackgroundResource(R.drawable.enter1);
                mCurrentFragmentIndex = FRAGMENT_EIGHT;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.iv_fragment09:
                offColorTv();
                iv_fragment09.setBackgroundResource(R.drawable.sisa1);
                mCurrentFragmentIndex = FRAGMENT_NINE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.iv_fragment10:
                offColorTv();
                iv_fragment10.setBackgroundResource(R.drawable.movie1);
                mCurrentFragmentIndex = FRAGMENT_TEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.iv_fragment11:
                offColorTv();
                iv_fragment11.setBackgroundResource(R.drawable.drama1);
                mCurrentFragmentIndex = FRAGMENT_ELEVEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.iv_fragment12:
                offColorTv();
                iv_fragment12.setBackgroundResource(R.drawable.enter1);
                mCurrentFragmentIndex = FRAGMENT_TWOWELVE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_livetv:
                offColorTv();
                tv_fragmentLive.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_LIVE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_livetv_enter:
                offColorTv();
                tv_fragmentLiveEnter.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_LIVE_ENTER;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.btn_closed:
                Intent marketLaunch1 = new Intent(Intent.ACTION_VIEW);
                Log.d(TAG, "  " + nextAppUrl);
                marketLaunch1.setData(Uri.parse(nextAppUrl));
                startActivity(marketLaunch1);
                break;
            case R.id.btn_refresh :
                //refreshAd();
                break;
            case R.id.tv_finish_app :
                if(alertDialog != null) alertDialog.cancel();
                finish();
                break;
            case R.id.tv_finish_app_cancel :
                if(alertDialog != null) alertDialog.cancel();
                break;

        }
    }

    private Fragment getFragment(int idx) {
        Fragment newFragment = null;
        Bundle args = new Bundle();
        args.putInt("adsCnt", adsCnt);

        switch (idx) {

            case FRAGMENT_ONE:
                newFragment = new FragmentO9o9();
                args.putString("baseUrl", fragment01Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWO:
                newFragment = new FragmentO9o9();
                args.putString("baseUrl", fragment02Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THREE:
                newFragment = new FragmentO9o9();
                args.putString("baseUrl", fragment03Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FOUR:
                newFragment = new FragmentO9o9Mubi();
                args.putString("baseUrl", fragment04Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FIVE:
                newFragment = new FragmentO9o9Mubi();
                args.putString("baseUrl", fragment05Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_SIX:
                newFragment = new FragmentMaruSearch();
                args.putString("baseUrl", fragment06Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_SEVEN:
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragment07Url);
                args.putString("type", "dr");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_EIGHT:
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragment08Url);
                args.putString("type", "en");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_NINE:
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragment09Url);
                args.putString("type", "cu");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TEN:
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragment10Url);
                args.putString("type", "mo");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ELEVEN:
                newFragment = new FragmentLinkMid();
                args.putString("baseUrl", fragment11Url);
                args.putString("type", "%7C1/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWOWELVE:
                newFragment = new FragmentLinkMid();
                args.putString("baseUrl", fragment12Url);
                args.putString("type", "%7C2/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LIVE:
                newFragment = new FragmentLive();
                args.putString("baseUrl", fragmentLive);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LIVE_ENTER:
                newFragment = new FragmentLive();
                args.putString("baseUrl", fragmentLiveEnter);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FAKE_MENU:
                newFragment = new FakeFragment();
                newFragment.setArguments(args);
                break;
            default:
                Log.d(TAG, "Unhandle case");
                newFragment = new FragmentMaruSearch();
                args.putString("baseUrl", fragment06Url);
                newFragment.setArguments(args);
                break;
        }

        return newFragment;
    }

    public void fragmentReplace(int reqNewFragmentIndex) {
        Fragment newFragment = null;
        Log.d(TAG, "fragmentReplace " + reqNewFragmentIndex);
        newFragment = getFragment(reqNewFragmentIndex);
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction(); // replace fragment
        transaction.replace(R.id.ll_fragment, newFragment);
        transaction.commit();   // Commit the transaction
    }

    public void offColorTv(){
        iv_fragment01.setBackgroundResource(R.drawable.drama);
        iv_fragment02.setBackgroundResource(R.drawable.enter);
        iv_fragment03.setBackgroundResource(R.drawable.sisa);
        iv_fragment04.setBackgroundResource(R.drawable.movie);
        iv_fragment05.setBackgroundResource(R.drawable.mid);
        iv_fragment06.setBackgroundResource(R.drawable.total);
        iv_fragment07.setBackgroundResource(R.drawable.drama);
        iv_fragment08.setBackgroundResource(R.drawable.enter);
        iv_fragment09.setBackgroundResource(R.drawable.sisa);
        iv_fragment10.setBackgroundResource(R.drawable.movie);
        iv_fragment11.setBackgroundResource(R.drawable.drama);
        iv_fragment12.setBackgroundResource(R.drawable.enter);
        //tv_fragmentLive.setBackgroundResource(R.drawable.fragment_borther);
        //tv_fragmentLiveEnter.setBackgroundResource(R.drawable.fragment_borther);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "main activity onResume, appStatus : " + appStatus);

        if((appStatus.equals("1")||appStatus.equals("99")) && d1 != null){
            d2 = new Date();
            long diff = d2.getTime() - d1.getTime();
            long min = diff / 1000 / 60 ;
            Log.d(TAG, "min : " + min);
            if(min >= 30){
                d1 = new Date();

                SharedPreferences pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언
                SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                editor.putString("adsCnt", "1"); //First라는 key값으로 id 데이터를 저장한다.
                editor.commit(); //완료한다.

                adFull();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(alertDialog != null) alertDialog.cancel();
        //refreshAd();
        //alertDialog.show();
    }

    public void adFull(){

    }

    // Back Key가 눌러졌을 때, CloseAd 호출
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            alertDialog.show();

            //showDefaultClosePopup();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showDefaultClosePopup() {
        new AlertDialog.Builder(this).setTitle("").setMessage("종료 하시겠습니까?")
                .setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("아니요",null)
                .show();
    }
}
