package com.zaffran.o9o9tvgittype3.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.zaffran.o9o9tvgittype3.R;
import com.zaffran.o9o9tvgittype3.activity.list.QooListActivity;
import com.zaffran.o9o9tvgittype3.adapter.ListDtamaAdapter;
import com.zaffran.o9o9tvgittype3.item.ListDramaItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class FragmentO9o9 extends Fragment implements View.OnClickListener {
    private final String TAG = " FragmentO9o9 - ";
    private ProgressDialog mProgressDialog;
    private ListView listView;
    private GetListView getListView = null;
    private String baseUrl = "";
    private ArrayList<ListDramaItem> listViewItemArr;
    //// paging ////
    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;
    private String lastPage = "1";

    private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    private int adn = 0;

    private String intentListUrl = "";
    private String intentImgUrl = "";
    private String intentTitle = "";

    // 중복 클릭 방지 시간 설정
    private static final long MIN_CLICK_INTERVAL=1500;
    private long mLastClickTime;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drama, container, false);

        baseUrl = getArguments().getString("baseUrl");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId(getResources().getString(R.string.full_main));
        interstitialAd.loadAd(adRequest);

        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.d(TAG, "success to load AD");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d(TAG, "failed to load AD");
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.d(TAG, "closed AD");
                interstitialAd.loadAd(new AdRequest.Builder().build());

                Intent intent = new Intent(getActivity(), QooListActivity.class);
                intent.putExtra("listUrl", intentListUrl);
                intent.putExtra("title", intentTitle);
                intent.putExtra("imgUrl", intentImgUrl);
                intent.putExtra("adsCnt", "2");
                startActivity(intent);

            }
        });

        listView = (ListView)view.findViewById(R.id.listview);

        //// paging ////
        tv_currentPage = (TextView)view.findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)view.findViewById(R.id.fr01_lastpage);
        tv_currentPage.setText("1");
        tv_lastPage.setText("1");
        preBtn = (Button)view.findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)view.findViewById(R.id.fr01_nextbtn);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        getListView = new GetListView();
        getListView.execute();

        return view;
    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<ListDramaItem>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            try {
                Log.d(TAG, "baseUrl : " + baseUrl + pageNum + "/");
                doc = Jsoup.connect(baseUrl + pageNum + "/").timeout(20000).get();

                Elements elements = doc.select(".board_list ul li");

                for(Element element: elements) {
                    String title = element.select(".bo_subject a").text();
                    String imgUrl = AddHttps(element.select(".bo_pf_img img").attr("src"));
                    String listUrl = element.select(".bo_subject a").attr("href").split("&page=")[0];

                    //Log.d(TAG, "title : " + title);
                    ListDramaItem listViewItemList = new ListDramaItem(title, imgUrl, listUrl);
                    listViewItemArr.add(listViewItemList);
                }

                ////////////// get lat page /////////////
                if(lastPage.equals("1")){
                    //Elements pages = doc.select(".pagination li");
                    lastPage = "40";
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(getActivity() != null){
                listView.setAdapter(new ListDtamaAdapter(getActivity(), listViewItemArr, R.layout.item_list_drama));

                //// paging ////
                tv_currentPage.setText(pageNum+"");
                tv_lastPage.setText(lastPage);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        long currentClickTime= SystemClock.uptimeMillis();
                        long elapsedTime=currentClickTime-mLastClickTime;
                        mLastClickTime=currentClickTime;
                        // 중복 클릭인 경우
                        if(elapsedTime<=MIN_CLICK_INTERVAL){
                            return;
                        }

                        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

                        if(adsCnt == 1){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.

                            intentListUrl = listViewItemArr.get(position).getListUrl();
                            intentImgUrl = listViewItemArr.get(position).getImgUrl();
                            intentTitle = listViewItemArr.get(position).getTitle();

                            interstitialAd.show();

                        } else {
                            intentListUrl = listViewItemArr.get(position).getListUrl();
                            intentImgUrl = listViewItemArr.get(position).getImgUrl();
                            intentTitle = listViewItemArr.get(position).getTitle();

                            Intent intent = new Intent(getActivity(), QooListActivity.class);
                            intent.putExtra("listUrl", intentListUrl);
                            intent.putExtra("title", intentTitle);
                            intent.putExtra("imgUrl", intentImgUrl);
                            intent.putExtra("adsCnt", "2");
                            startActivity(intent);
                        }
                    }
                });
            }

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getListView != null){
            getListView.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getListView != null){
            getListView.cancel(true);
        }
    }

    public static String AddHttps(String baseUrl){
        String resultUrl = baseUrl;
        if(!baseUrl.contains("https")) resultUrl = "https:" + resultUrl;

        return resultUrl;
    }

}
